package educacionit.mtanaro.javabootcamp.entregable5.com.mensajes;

public class ExceptionMessages {

    public static final String SUPERA_TAMANIO="La cantidad ingresada supera el tamanio maximo establecido para esta accion";
    public static final String FORMATO_NOMBRE="El formato del nombre contiene al menos un numero o es null";
    public static final String FORMATO_APELLIDO="El formato del apellido contiene al menos un numero o es null";

}
