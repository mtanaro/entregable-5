package educacionit.mtanaro.javabootcamp.entregable5.com.enums;

public enum Lenguaje {
    JAVA,
    PYTHON,
    C_PLUS_PLUS,
    C_SHARP,
    GO,
    PHP,
    JAVASCRIPT,
    C,
    ASSEMBLER,
    ADA,
    COBOL,
    PLSQL,
    NODE,
    KOTLIN

}
