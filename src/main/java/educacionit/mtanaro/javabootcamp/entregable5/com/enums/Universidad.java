package educacionit.mtanaro.javabootcamp.entregable5.com.enums;

public enum Universidad {
    UBA,
    CAECE,
    USAM,
    UTN,
    SALVADOR,
    U_3_DE_FEBRERO,
    UCES,
    CATOLICA,
    AUSTRAL,
    MARINA_MERCANTE

}
