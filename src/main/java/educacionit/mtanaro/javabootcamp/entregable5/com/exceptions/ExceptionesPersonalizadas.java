package educacionit.mtanaro.javabootcamp.entregable5.com.exceptions;

public class ExceptionesPersonalizadas extends Exception {

    public ExceptionesPersonalizadas(String errorMessage) {
        super(errorMessage);
    }

}
