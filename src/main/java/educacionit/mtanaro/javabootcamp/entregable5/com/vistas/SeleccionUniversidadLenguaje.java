package educacionit.mtanaro.javabootcamp.entregable5.com.vistas;

import static educacionit.mtanaro.javabootcamp.entregable5.com.constantes.Constantes.*;
import static educacionit.mtanaro.javabootcamp.entregable5.com.constantes.Constantes.NUMBER_TAGS;
import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Lenguaje.*;

public class SeleccionUniversidadLenguaje {
    public static void SeleccionLenguaje() {
        System.out.println(MENU_TAG);
        System.out.println("1 - " + JAVA);
        System.out.println("2 - " + PYTHON);
        System.out.println("3 - " + C_PLUS_PLUS);
        System.out.println("4 - " + C_SHARP);
        System.out.println("5 - " + GO);
        System.out.println("6 - " + PHP);
        System.out.println("7 - " + VOLVER_ATRAS);
        System.out.println("8 - " + SALIR);
        System.out.println(NUMBER_TAGS);
    }
}
