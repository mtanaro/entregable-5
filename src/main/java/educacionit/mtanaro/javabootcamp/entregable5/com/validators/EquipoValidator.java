package educacionit.mtanaro.javabootcamp.entregable5.com.validators;

import educacionit.mtanaro.javabootcamp.entregable5.com.Menues.MenuJugadores;
import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.Equipo;
import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.EquipoParent;

import java.util.Map;

public class EquipoValidator {

    public static boolean checkTamanioEquipo(int size, EquipoParent equipo) {
        return equipo.MAX >= size;
    }

    public static boolean buscarEquipo(String nombreEquipo, Map<String, Equipo> listadoDeEquipos) {

        return listadoDeEquipos.get(nombreEquipo) != null;
    }

    public static boolean esEquipo(Object o) {
        return o instanceof Equipo;
    }
}

