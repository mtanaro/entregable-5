package educacionit.mtanaro.javabootcamp.entregable5.com.equipos;
import educacionit.mtanaro.javabootcamp.entregable5.com.exceptions.ExceptionesPersonalizadas;
import educacionit.mtanaro.javabootcamp.entregable5.com.participantes.Programador;
import educacionit.mtanaro.javabootcamp.entregable5.com.validators.EquipoValidator;

import java.util.SortedSet;
import java.util.TreeSet;

import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.ExceptionMessages.SUPERA_TAMANIO;


public abstract class EquipoParent implements LimitesJugadores  {
    private SortedSet<Programador> programadores;
    private String lenguaje;
    private String universidad;
    private int tamanio;
    private String nombreEquipo;
    protected EquipoParent(String lenguaje, String universidad, int tamanio, String nombreEquipo) {
        this.programadores = new TreeSet<>();
        this.lenguaje = lenguaje;
        this.universidad = universidad;
        this.tamanio = tamanio;
        this.nombreEquipo=nombreEquipo;
    }
    public String getEquipoNombre(){
        return this.nombreEquipo;
    }
    protected EquipoParent(String nombre){
        this.nombreEquipo=nombre;
    }
    public SortedSet<Programador> getProgramadores() {
        return programadores;
    }

    public void agregarProgramadores(SortedSet<Programador> programadores){
        try {
            if (EquipoValidator.checkTamanioEquipo(this.getProgramadores().size() + programadores.size(),this))
                this.programadores = programadores;
            else throw new ExceptionesPersonalizadas(SUPERA_TAMANIO);
        } catch (ExceptionesPersonalizadas e) {
            System.out.println(e.getMessage());
        }

    }

    public void agregarProgramadores(Programador programador) {
        this.programadores.add(programador);
        this.tamanio=getProgramadores().size();
    }

    public String getLenguaje() {
        return lenguaje;
    }

    public void setLenguaje(String lenguaje) {
        this.lenguaje = lenguaje;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public int getTamanio() {
        return programadores.size();
    }

    @Override
    public String toString() {
        return "EquipoParent{" +
                "programadores=" + programadores +
                ", lenguaje='" + lenguaje + '\'' +
                ", universidad='" + universidad + '\'' +
                ", tamanio=" + tamanio +
                ", nombreEquipo='" + nombreEquipo + '\'' +
                '}';
    }
}
