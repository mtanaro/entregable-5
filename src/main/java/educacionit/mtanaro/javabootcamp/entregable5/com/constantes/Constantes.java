package educacionit.mtanaro.javabootcamp.entregable5.com.constantes;

public class Constantes {
    public static final String MENU_TAG = "Seleccione la accion que corresponda: ";
    public static final String MENU_CREAR_EQUIPO = "Crear nuevo equipo";
    public static final String MENU_AGREGAR_JUGADORES = "Agregar jugadores";
    public static final String LISTAR_JUGADORES = "Listar Jugadores existentes";
    public static final String LISTAR_EQUIPOS = "Listar equipos";
    public static final String LISTAR_TODO = "Listar todo";
    public static final String SALIR = "Salir del sistema";
    public static final String NUMBER_TAGS = "#############################################";
    public static final String VOLVER_ATRAS = "Volver atras";


}