package educacionit.mtanaro.javabootcamp.entregable5.com.Menues;

import educacionit.mtanaro.javabootcamp.entregable5.com.exceptions.ExceptionesPersonalizadas;
import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.Equipo;
import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.LimitesJugadores;
import educacionit.mtanaro.javabootcamp.entregable5.com.participantes.Programador;
import educacionit.mtanaro.javabootcamp.entregable5.com.validators.MenuValidator;

import java.util.Scanner;

import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.ExceptionMessages.SUPERA_TAMANIO;

public class MenuJugadores {

    public static void menuAgregarJugadores(Equipo equipo, Scanner scan) {
        String nombre;
        String apellido;
        Object cantidad=null;
        int indexParticipantes = 0;
        do {
            System.out.println("Ingrese la cantidad de jugadores que desea agregar:");
            try {
                cantidad = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("No es formato numerico, ingrese nuevamente");
            }
        } while (!MenuValidator.esNumero(cantidad));


        try {
            if (equipo.getProgramadores().size() + (Integer)cantidad <= LimitesJugadores.MAX) {

                do {

                    System.out.println("Ingrese el nombre jugador "+indexParticipantes+1 +" que desea agregar:");

                    nombre = scan.nextLine();

                    System.out.println("Ingrese el apellido jugador "+indexParticipantes+1 +"  que desea agregar:");

                    apellido = scan.nextLine();
                    equipo.agregarProgramadores(new Programador(nombre, apellido));
                    indexParticipantes++;
                } while ((Integer)cantidad > indexParticipantes);


            } else
                throw new ExceptionesPersonalizadas(SUPERA_TAMANIO);
        } catch (ExceptionesPersonalizadas e) {
            System.out.println(e.getMessage());
        }
    }
}
