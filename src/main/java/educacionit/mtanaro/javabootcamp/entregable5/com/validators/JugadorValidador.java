package educacionit.mtanaro.javabootcamp.entregable5.com.validators;

import educacionit.mtanaro.javabootcamp.entregable5.com.exceptions.ExceptionesPersonalizadas;

import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.ExceptionMessages.FORMATO_NOMBRE;

public class JugadorValidador {

    public static boolean validarFormatoNYA(String nombreOApellido) {
        try {
            if (nombreOApellido.matches("^[a-zA-z ]*$"))
                return true;
            else throw new ExceptionesPersonalizadas(FORMATO_NOMBRE);
        } catch (ExceptionesPersonalizadas e) {
            System.out.println(e.getMessage());
        }
        return false;
    }
}
