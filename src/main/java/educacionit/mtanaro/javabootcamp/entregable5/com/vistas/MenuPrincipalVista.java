package educacionit.mtanaro.javabootcamp.entregable5.com.vistas;


import static educacionit.mtanaro.javabootcamp.entregable5.com.constantes.Constantes.*;

public class MenuPrincipalVista {
    public static void menuPrincipal() {
        System.out.println(MENU_TAG);
        System.out.println("1 - " + MENU_CREAR_EQUIPO);
        System.out.println("2 - " + MENU_AGREGAR_JUGADORES);
        System.out.println("3 - " + LISTAR_JUGADORES);
        System.out.println("4 - " + LISTAR_EQUIPOS);
        System.out.println("5 - " + SALIR);
        System.out.println(NUMBER_TAGS);
    }
}

