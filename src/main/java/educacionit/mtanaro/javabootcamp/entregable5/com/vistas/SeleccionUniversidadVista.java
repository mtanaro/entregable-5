package educacionit.mtanaro.javabootcamp.entregable5.com.vistas;

import static educacionit.mtanaro.javabootcamp.entregable5.com.constantes.Constantes.*;
import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Universidad.*;

public class SeleccionUniversidadVista {
    public static void SeleccionUniversidad() {
        System.out.println(MENU_TAG);
        System.out.println("1 - " + UBA);
        System.out.println("2 - " +  CAECE);
        System.out.println("3 - " + USAM);
        System.out.println("4 - " + UTN);
        System.out.println("5 - " + SALVADOR);
        System.out.println("6 - " + U_3_DE_FEBRERO);
        System.out.println("7 - " + UCES);
        System.out.println("8 - " + CATOLICA);
        System.out.println("9 - " + AUSTRAL);
        System.out.println("10 - " + MARINA_MERCANTE);
        System.out.println("11 - " + SALIR);
        System.out.println(NUMBER_TAGS);
    }
}
