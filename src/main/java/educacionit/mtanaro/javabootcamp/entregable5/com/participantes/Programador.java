package educacionit.mtanaro.javabootcamp.entregable5.com.participantes;

import educacionit.mtanaro.javabootcamp.entregable5.com.validators.JugadorValidador;


public class Programador implements Comparable<Programador> {
    private String apellido;
    private String nombre;

    public Programador(String nombre, String apellido) {

        setNombre(nombre);
        setApellido(apellido);

    }

    public String getNombre() {
        return nombre;
    }

    private void setNombre(String nombre) {
        if (JugadorValidador.validarFormatoNYA(nombre))
            this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    private void setApellido(String apellido) {
        if (JugadorValidador.validarFormatoNYA(apellido))
            this.apellido=apellido;
    }

    @Override
    public String toString() {
        return "Programador{" + "appellido='" + apellido + '\'' + ", nombre='" + nombre + '\'' + '}';
    }


    @Override
    public int compareTo(Programador o) {
        if (o.nombre.equals(this.nombre) && o.apellido.equals(this.apellido)) return 0;
        else return 1;
    }
}
