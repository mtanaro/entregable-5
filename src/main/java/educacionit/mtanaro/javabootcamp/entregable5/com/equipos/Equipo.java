package educacionit.mtanaro.javabootcamp.entregable5.com.equipos;

import educacionit.mtanaro.javabootcamp.entregable5.com.participantes.Programador;

public class Equipo extends EquipoParent {


    public Equipo(String lenguaje, String universidad, int tamanio, String nombreEquipo) {
        super(lenguaje, universidad, tamanio, nombreEquipo);
    }

    public Equipo(String nombre) {
        super("", "", 0, nombre);
    }


    @Override
    public void agregarProgramadores(Programador programador) {
        super.agregarProgramadores(programador);
    }
}
