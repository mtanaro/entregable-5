package educacionit.mtanaro.javabootcamp.entregable5.com.Menues;

import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.Equipo;
import educacionit.mtanaro.javabootcamp.entregable5.com.validators.EquipoValidator;
import educacionit.mtanaro.javabootcamp.entregable5.com.vistas.MenuPrincipalVista;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.GeneralMessage.OPCION_INCORRECTA;


public class MenuPrincipal {

    public static void interfazDeAccion(Map<String, Equipo> listadoEquipos) {

        Scanner leer = new Scanner(System.in);
        menuInicial(leer, listadoEquipos);

    }

    public static void menuInicial(Scanner scan, Map<String, Equipo> listadoEquipos) {

        String menuNumber = "0";
        String input = "";
        String nombreEquipo;
        HashMap<String, String> inputMap = new HashMap<>();


        while (!menuNumber.equals("6")) {
            MenuPrincipalVista.menuPrincipal();
            System.out.println("Ingrese La opcion deseada:");
            menuNumber = scan.next();

            switch (menuNumber) {

                case "1" -> {

                    System.out.println("Ingrese el nombre del equipo que desea crear:");
                    scan.nextLine();
                    nombreEquipo = scan.nextLine();
                    inputMap.put("nombreEquipo", nombreEquipo);

                    listadoEquipos.put(inputMap.get("nombreEquipo"), new Equipo(inputMap.get("nombreEquipo")));
                    MenuJugadores.menuAgregarJugadores(listadoEquipos.get(nombreEquipo), scan);
                    MenuLenguaje.menuAgregarLenguaje(listadoEquipos.get(nombreEquipo), scan);
                    MenuUniversidad.menuAgregarUniversidad(listadoEquipos.get(nombreEquipo), scan);

                    System.out.println("Se ha creado el equipo con las siguientes caracteristicas: ");
                    System.out.println(listadoEquipos.get(nombreEquipo));

                    break;
                }
                case "2" -> {
                    System.out.println("Ingrese el nombre del equipo al cual desea añadir jugadores: ");
                    nombreEquipo = scan.next() + scan.nextLine();
                    Object equipo = buscarEquipo(nombreEquipo, listadoEquipos);

                    if (EquipoValidator.esEquipo(equipo)) {
                        MenuJugadores.menuAgregarJugadores(((Equipo) equipo), scan);
                        System.out.println("El equipo : "+nombreEquipo+ " Esta conformado de la siguiente forma: ");
                        System.out.println(listadoEquipos.get(nombreEquipo));
                    } else System.out.println("No hay jugadores ni equipo para esta peticion");

                    break;
                }
                case "3" -> {
                    System.out.println("Ingrese el nombre del equipo cuyos jugadores desea listar:");
                    input = scan.next() + scan.nextLine();

                    Object equipo = buscarEquipo(input, listadoEquipos);

                    if (EquipoValidator.esEquipo(equipo)) {
                        System.out.println("Listado de equipos: ");
                        System.out.println(((Equipo) equipo).getProgramadores());
                    } else System.out.println("No hay jugadores ni equipo para esta peticion");


                   break;
                }
                case "4" -> {
                    if (!listadoEquipos.isEmpty())
                        System.out.println(listadoEquipos);
                    else
                        System.out.println("No se se han dado de alta equipos");

                    break;
                }
                case "5" -> {
                    return;
                }
                case "6" -> {
                    System.out.println("Hasta la vista baby...");
                    scan.close();
                    System.exit(0);
                }
                default -> System.out.println(OPCION_INCORRECTA);
            }
        }


    }


    private static Object buscarEquipo(String nombreEquipo, Map<String, Equipo> listadoEquipos) {
        if (EquipoValidator.buscarEquipo(nombreEquipo, listadoEquipos))
            return listadoEquipos.get(nombreEquipo);
        else
            return new Object();
    }


}