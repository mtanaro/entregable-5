package educacionit.mtanaro.javabootcamp.entregable5.com.Menues;

import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.Equipo;
import educacionit.mtanaro.javabootcamp.entregable5.com.vistas.SeleccionUniversidadLenguaje;

import java.util.Scanner;

import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Lenguaje.*;
import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Lenguaje.PHP;
import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.GeneralMessage.OPCION_INCORRECTA;

public class MenuLenguaje {

    public static void menuAgregarLenguaje(Equipo equipo, Scanner scan) {
        String menuNumber = "0";

        SeleccionUniversidadLenguaje.SeleccionLenguaje();

        System.out.println("Ingrese el lenguaje que usará el equipo:");

        while (!menuNumber.equals("6")) {
            menuNumber = scan.next();
            switch (menuNumber) {

                case "1" -> {
                    equipo.setLenguaje(JAVA.name());
                    return;
                }
                case "2" -> {
                    equipo.setLenguaje(PYTHON.name());
                    return;
                }
                case "3" -> {
                    equipo.setLenguaje(C_PLUS_PLUS.name());
                    return;
                }
                case "4" -> {
                    equipo.setLenguaje(C_SHARP.name());
                    return;
                }
                case "5" -> {
                    equipo.setLenguaje(GO.name());
                    return;
                }
                case "6" -> {
                    equipo.setLenguaje(PHP.name());
                    return;
                }
                case "7" -> {
                    return;
                }
                case "8" -> {
                    scan.close();
                    System.exit(0);
                }

                default -> System.out.println(OPCION_INCORRECTA);
            }
        }
    }
}
