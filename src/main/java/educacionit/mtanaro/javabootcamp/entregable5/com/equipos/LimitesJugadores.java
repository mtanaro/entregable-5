package educacionit.mtanaro.javabootcamp.entregable5.com.equipos;

public interface LimitesJugadores {

    public static final int MIN=2;
    public static final int MAX=3;

}
