package educacionit.mtanaro.javabootcamp.entregable5.com.Menues;

import educacionit.mtanaro.javabootcamp.entregable5.com.equipos.Equipo;
import educacionit.mtanaro.javabootcamp.entregable5.com.vistas.SeleccionUniversidadVista;

import java.util.Scanner;

import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Universidad.*;
import static educacionit.mtanaro.javabootcamp.entregable5.com.enums.Universidad.MARINA_MERCANTE;
import static educacionit.mtanaro.javabootcamp.entregable5.com.mensajes.GeneralMessage.OPCION_INCORRECTA;

public class MenuUniversidad {
    public static void menuAgregarUniversidad(Equipo equipo, Scanner scan) {
        String menuNumber;
        System.out.println("Seleccione la universidad a representar:");
        SeleccionUniversidadVista.SeleccionUniversidad();
        menuNumber=scan.nextLine();
        while (!menuNumber.equals("6")) {
            menuNumber = scan.next();
            switch (menuNumber) {

                case "1" -> {
                    equipo.setUniversidad(UBA.name());
                    return;
                }
                case "2" -> {
                    equipo.setUniversidad(CAECE.name());
                    return;
                }
                case "3" -> {
                    equipo.setUniversidad(USAM.name());
                    return;
                }
                case "4" -> {
                    equipo.setUniversidad(UTN.name());
                    return;
                }
                case "5" -> {
                    equipo.setUniversidad(SALVADOR.name());
                    return;
                }
                case "6" -> {
                    equipo.setUniversidad(U_3_DE_FEBRERO.name());
                    return;
                }

                case "7" -> {
                    equipo.setUniversidad(UCES.name());
                    return;
                }
                case "8" -> {
                    equipo.setUniversidad(CATOLICA.name());
                    return;
                }
                case "9" -> {
                    equipo.setUniversidad(AUSTRAL.name());
                    return;
                }
                case "10" -> {
                    equipo.setUniversidad(MARINA_MERCANTE.name());
                    return;
                }

                case "11" -> {
                    scan.close();
                    System.exit(0);
                }

                default -> System.out.println(OPCION_INCORRECTA);
            }
        }
    }
}
